import React from 'react';
import createStore from './src/redux';
import Toast from 'react-native-toast-message';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import ContactNavigation from './src/navigation/ContactNavigation';

const store = createStore();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <ContactNavigation />
      </NavigationContainer>
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </Provider>
  );
}
