import React, {useState} from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal'

const ModalViewImage = props => {
  const { viewedImage, setViewedImage} = props
  return (
    <Modal
        isVisible={!!viewedImage}
        onBackdropPress={() => {
          setViewedImage(null)
        }}
      >
        <TouchableOpacity style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }} onPress={() => {
          setViewedImage(null)
        }} activeOpacity={1}>
          <View style={{
            margin: 20,
            borderRadius: 20,
            alignItems: "center",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 4,
            elevation: 5
          }}>
            <Image source={{uri: viewedImage}} style={{width: 300, height: 500}} resizeMode="contain" />
          </View>
        </TouchableOpacity>
      </Modal>
  )
}
export default ModalViewImage;

const styles = StyleSheet.create({});