import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const InisialImage = (props) => {
  const { name, detail } = props
  
  return (
    <View style={{height: detail ? 100 : 35, width: detail ? 100 : 35, borderRadius: detail ? 50: 6, backgroundColor: 'grey', alignItems: 'center', alignContent: 'center', justifyContent: 'center'}}>
      <Text style={{ color: 'white', fontSize:detail ? 35 : 12, fontWeight: detail ? 'bold' : null}}>
        {typeof name === 'string' ? name[0].toUpperCase() : ''}
      </Text>
    </View>
  )
}

export default InisialImage

const styles = StyleSheet.create({})
