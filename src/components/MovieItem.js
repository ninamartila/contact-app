import React, {useState} from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

const MovieItem = props => {
  const { item, setViewedImage, onPress } = props;
  return (
    <TouchableOpacity 
    onPress={onPress}
    >
      <View style={{flexDirection: 'column', borderBottomWidth: 1, borderBottomColor: "#ccc", borderBottomRadius: 8}}>
        <View style={{flexDirection: 'row', margin: 20}}>
          <TouchableOpacity
            onPress={() => setViewedImage(item.Poster)}
          >
            <Image source={{uri: item.Poster}} style={{height: 100, width: 100}} />
          </TouchableOpacity>
          <View style={{paddingLeft: 10, flex: 1}}>
            <Text style={{fontWeight: 'bold', fontSize: 20, color: 'black'}}>{item.Title}</Text>
            <Text style={{fontSize: 15}}>{item.Type}</Text>
          </View>
          <View style={{width: 80}}>
            <Text style={{textAlign: 'right',fontSize: 15, paddingTop: 80}}>{item.Year}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}
export default MovieItem;

const styles = StyleSheet.create({});