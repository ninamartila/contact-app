import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import InisialImage from './InisialImage';
import { ListItem, Avatar } from 'react-native-elements';

const ContactItem = (props) => {
  const { item, onPress } = props;
  console.log({ item });
  return (
    <ListItem onPress={onPress} bottomDivider>
      <Avatar
        title={item.firstName[0]}
        containerStyle={styles.avatarContainerStyle}
        source={item.photo && item.photo !== 'N/A' && { uri: item.photo }}
        avatarStyle={styles.avatarStyle}
      />
      <ListItem.Content>
        <ListItem.Title>
          {item.firstName}
          {item.lastName && ` ${item.lastName}`}
        </ListItem.Title>
        <ListItem.Subtitle>{item.age} years old</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>
  );
};
export default ContactItem;

const styles = StyleSheet.create({
  avatarContainerStyle: { backgroundColor: 'grey', borderRadius: 6 },
  avatarStyle: { borderRadius: 6 },
});
