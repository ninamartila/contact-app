import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ContactListScreen from '../views/Contact/ContactListScreen';
import ContactDetailScreen from '../views/Contact/ContactDetailScreen';
import ContactAddScreen from '../views/Contact/ContactAddScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { StyleSheet, TouchableOpacity } from 'react-native';
const Stack = createStackNavigator();

const ContactNavigation = () => {
  return (
    <Stack.Navigator initialRouteName="ContactList">
      <Stack.Screen
        name="ContactList"
        component={ContactListScreen}
        options={{ title: 'Contact' }}
      />
      <Stack.Screen
        name="ContactDetail"
        component={ContactDetailScreen}
        options={{ title: 'Contact Detail' }}
      />
      <Stack.Screen name="ContactAdd" component={ContactAddScreen} />
    </Stack.Navigator>
  );
};

export default ContactNavigation;

const styles = StyleSheet.create({
  buttonRightContainer: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
