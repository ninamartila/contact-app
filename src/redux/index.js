/* eslint-disable import/no-self-import */
/* eslint-disable import/no-cycle */
/* eslint-disable global-require */
import { combineReducers } from 'redux';
import configureStore from './CreateStore';

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  contact: require('./ContactRedux').reducer,
});

export default () => {
  let finalReducers = reducers;

  let { store } = configureStore(finalReducers);

  return store;
};
