import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  //getContact
  getContactRequest: ['data'],
  getContactSuccess: ['payload'],
  getContactFailure: ['error'],
  //PostContact
  postContactRequest: ['data'],
  postContactSuccess: ['payload'],
  postContactFailure: ['error'],
  //updateContact
  updateContactRequest: ['data'],
  updateContactSuccess: ['payload'],
  updateContactFailure: ['error'],
  //deleteContact
  deleteContactRequest: ['data'],
  deleteContactSuccess: ['payload'],
  deleteContactFailure: ['error'],
  //getContactDetail
  getContactDetailRequest: ['data'],
  getContactDetailSuccess: ['payload'],
  getContactDetailFailure: ['error'],
});

export const ContactTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const DEFAULT_LIST_STATE = {
  data: null,
  fetching: false,
  payload: [],
  error: null,
};

export const DEFAULT_STATE = {
  data: null,
  fetching: false,
  payload: null,
  error: null,
};

export const INITIAL_STATE = Immutable({
  getContact: DEFAULT_LIST_STATE,
  postContact: DEFAULT_STATE,
  updateContact: DEFAULT_STATE,
  deleteContact: DEFAULT_STATE,
  getContactDetail: DEFAULT_STATE,
});

export const ContactSelectors = {};
/* ------------- Reducers ------------- */

export const getContactRequest = (state, { data }) =>
  state.merge({
    ...state,
    getContact: {
      fetching: true,
      data,
    },
  });

// successful api lookup
export const getContactSuccess = (state, { payload }) => {
  const newData = payload;
  return state.merge({
    ...state,
    getContact: {
      ...state.getContact,
      fetching: false,
      error: null,
      payload: newData,
    },
  });
};

// Something went wrong somewhere.
export const getContactFailure = (state, { error }) => {
  return state.merge({
    ...state,
    getContact: {
      ...state.getContact,
      fetching: false,
      error,
    },
  });
};

export const postContactRequest = (state, { data }) =>
  state.merge({
    ...state,
    postContact: {
      ...state.postContact,
      fetching: true,
      data,
    },
  });

// successful api lookup
export const postContactSuccess = (state, { payload }) => {
  return state.merge({
    ...state,
    postContact: {
      ...state.postContact,
      fetching: false,
      error: null,
      payload,
    },
  });
};

// Something went wrong somewhere.
export const postContactFailure = (state, { error }) => {
  return state.merge({
    ...state,
    postContact: {
      ...state.postContact,
      fetching: false,
      error,
    },
  });
};

export const updateContactRequest = (state, { data }) =>
  state.merge({
    ...state,
    updateContact: {
      ...state.updateContact,
      fetching: true,
      data,
    },
  });

// successful api lookup
export const updateContactSuccess = (state, { payload }) => {
  return state.merge({
    ...state,
    updateContact: {
      ...state.updateContact,
      fetching: false,
      error: null,
      payload,
    },
  });
};

// Something went wrong somewhere.
export const updateContactFailure = (state, { error }) => {
  return state.merge({
    ...state,
    updateContact: {
      ...state.updateContact,
      fetching: false,
      error,
    },
  });
};

export const deleteContactRequest = (state, { data }) =>
  state.merge({
    ...state,
    deleteContact: {
      ...state.deleteContact,
      fetching: true,
      data,
    },
  });

// successful api lookup
export const deleteContactSuccess = (state, { payload }) => {
  return state.merge({
    ...state,
    deleteContact: {
      ...state.deleteContact,
      fetching: false,
      error: null,
      payload,
    },
  });
};

// Something went wrong somewhere.
export const deleteContactFailure = (state, { error }) => {
  return state.merge({
    ...state,
    deleteContact: {
      ...state.deleteContact,
      fetching: false,
      error,
    },
  });
};

export const getContactDetailRequest = (state, { data }) =>
  state.merge({
    ...state,
    getContactDetail: {
      ...state.getContactDetailContact,
      fetching: true,
      data,
    },
  });

// successful api lookup
export const getContactDetailSuccess = (state, { payload }) => {
  console.log({ payload });
  return state.merge({
    ...state,
    getContactDetail: {
      ...state.getContactDetail,
      fetching: false,
      error: null,
      payload: payload.data,
    },
  });
};

// Something went wrong somewhere.
export const getContactDetailFailure = (state, { error }) => {
  return state.merge({
    ...state,
    getContactDetail: {
      ...state.getContactDetail,
      fetching: false,
      error,
    },
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_CONTACT_REQUEST]: getContactRequest,
  [Types.GET_CONTACT_SUCCESS]: getContactSuccess,
  [Types.GET_CONTACT_FAILURE]: getContactFailure,
  [Types.POST_CONTACT_REQUEST]: postContactRequest,
  [Types.POST_CONTACT_SUCCESS]: postContactSuccess,
  [Types.POST_CONTACT_FAILURE]: postContactFailure,
  [Types.UPDATE_CONTACT_REQUEST]: updateContactRequest,
  [Types.UPDATE_CONTACT_SUCCESS]: updateContactSuccess,
  [Types.UPDATE_CONTACT_FAILURE]: updateContactFailure,
  [Types.DELETE_CONTACT_REQUEST]: deleteContactRequest,
  [Types.DELETE_CONTACT_SUCCESS]: deleteContactSuccess,
  [Types.DELETE_CONTACT_FAILURE]: deleteContactFailure,
  [Types.GET_CONTACT_DETAIL_REQUEST]: getContactDetailRequest,
  [Types.GET_CONTACT_DETAIL_SUCCESS]: getContactDetailSuccess,
  [Types.GET_CONTACT_DETAIL_FAILURE]: getContactDetailFailure,
});
