import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// creates the store
export default (rootReducer) => {
  /* ------------- Redux Configuration ------------- */

  const createAppropriateStore = createStore;
  const store = createAppropriateStore(rootReducer, applyMiddleware(thunk));

  return {
    store,
  };
};
