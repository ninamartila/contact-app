import axios from 'axios';
import Toast from 'react-native-toast-message';
import ContactActions from './ContactRedux';

export function getContact() {
  return async (dispatch) => {
    dispatch(ContactActions.getContactRequest());
    try {
      const response = await axios.get(
        `https://simple-contact-crud.herokuapp.com/contact`,
      );
      if (response.status >= 200 && response.status < 300) {
        dispatch(ContactActions.getContactSuccess(response.data.data));
      } else {
        throw response;
      }
    } catch (error) {
      console.log({ error });
      dispatch(ContactActions.getContactFailure(error));
    }
  };
}

export function postContact(param, callback) {
  return async (dispatch) => {
    dispatch(ContactActions.postContactRequest());
    console.log({ param });
    try {
      const response = await axios.post(
        `https://simple-contact-crud.herokuapp.com/contact`,
        {
          ...param,
        },
      );
      if (response.status >= 200 && response.status < 300) {
        console.log({ response });
        dispatch(ContactActions.postContactSuccess(response.data));
        if (callback) callback();
      } else {
        throw response;
      }
    } catch (error) {
      Toast.show({
        autoHide: true,
        position: 'bottom',
        text1: 'Error',
        text2: 'Something wrong when create new contact!',
        type: 'error',
      });
      console.log({ error });
      dispatch(ContactActions.postContactFailure(error));
    }
  };
}

export function updateContact(id, param, callback) {
  return async (dispatch) => {
    dispatch(ContactActions.updateContactRequest());
    try {
      console.log({ id, param });
      const response = await axios.put(
        `https://simple-contact-crud.herokuapp.com/contact/${id}`,
        {
          ...param,
        },
      );
      if (response.status >= 200 && response.status < 300) {
        dispatch(ContactActions.updateContactSuccess(response.data));
        if (callback) callback();
      } else {
        throw response;
      }
    } catch (error) {
      Toast.show({
        autoHide: true,
        position: 'bottom',
        text1: 'Error',
        text2: 'Something wrong when update contact!',
        type: 'error',
      });
      console.log({ error });
      dispatch(ContactActions.updateContactFailure(error));
    }
  };
}

export function deleteContact(id, callback) {
  return async (dispatch) => {
    console.log({ id });
    try {
      const response = await axios.delete(
        `https://simple-contact-crud.herokuapp.com/contact/${id}`,
      );
      if (response.status >= 200 && response.status < 300) {
        dispatch(ContactActions.deleteContactSuccess(response.data));
        if (callback) callback();
      } else {
        throw response;
      }
    } catch (error) {
      Toast.show({
        autoHide: true,
        position: 'bottom',
        text1: 'Error',
        text2: 'Something wrong when delete contact!',
        type: 'error',
      });
      console.log({ error });
      dispatch(ContactActions.deleteContactFailure(error));
    }
  };
}

export function getContactDetail(id, callback) {
  return async (dispatch) => {
    dispatch(ContactActions.getContactDetailRequest());
    try {
      const response = await axios.get(
        `https://simple-contact-crud.herokuapp.com/contact/${id}`,
      );
      if (response.status >= 200 && response.status < 300) {
        dispatch(ContactActions.getContactDetailSuccess(response.data));
        if (callback) callback();
      } else {
        throw response;
      }
    } catch (error) {
      console.log({ error });
      dispatch(ContactActions.getContactDetailFailure(error));
    }
  };
}
