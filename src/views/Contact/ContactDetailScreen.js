import { useNavigation } from '@react-navigation/core';
import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { ListItem, Avatar } from 'react-native-elements';
import InisialImage from '../../components/InisialImage';
import {
  deleteContact,
  getContact,
  getContactDetail,
} from '../../redux/ContactThunk';

const ContactDetailScreen = (props) => {
  const navigation = useNavigation();
  const {
    route,
    deleteContactRequest,
    getContactRequest,
    getContactDetailRequest,
    getContactDetail,
  } = props;
  const { item } = route.params;

  useEffect(() => {
    getContactDetailRequest(item.id);
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={styles.headerRight}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ContactAdd', getContactDetail.payload);
            }}
            style={styles.buttonRightContainer}
          >
            <Icon name="edit" size={30} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Alert.alert(
                'Delete Contact',
                'Are you sure you want to remove this contact?',
                [
                  {
                    text: 'Cancel',
                    onPress: () => {
                      console.log('Cancel Pressed');
                    },
                  },
                  {
                    text: 'Ok',
                    onPress: () => {
                      deleteContactRequest(getContactDetail.payload.id, () => {
                        navigation.goBack();
                        getContactRequest();
                      });
                    },
                    style: 'destructive',
                  },
                ],
                {
                  cancelable: true,
                },
              );
            }}
            style={styles.buttonRightContainer}
          >
            <Icon name="delete" size={30} />
          </TouchableOpacity>
        </View>
      ),
    });
  }, [getContactDetail.payload]);

  return (
    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={getContactDetail.fetching}
          onRefresh={() => {
            getContactDetailRequest(item.id);
          }}
        />
      }
      style={styles.container}
    >
      {getContactDetail.payload && (
        <Fragment>
          <View style={styles.content}>
            <Avatar
              size={100}
              title={getContactDetail.payload.firstName[0]}
              containerStyle={styles.avatarContainerStyle}
              source={
                getContactDetail.payload.photo &&
                getContactDetail.payload.photo !== 'N/A' && {
                  uri: getContactDetail.payload.photo,
                }
              }
              avatarStyle={styles.avatarStyle}
            />
            <View style={styles.contentName}>
              <Text style={styles.nameDetail}>
                {getContactDetail.payload.firstName}
                {getContactDetail.payload.lastName &&
                  ` ${getContactDetail.payload.lastName}`}
              </Text>
            </View>
          </View>
          <View style={styles.contentDetail}>
            <View>
              <Text style={styles.textName}>First Name</Text>
              <Text style={styles.textName}>Last Name</Text>
              <Text style={styles.textName}>Age</Text>
            </View>
            <View style={styles.flex}>
              <Text>: {getContactDetail.payload.firstName}</Text>
              <Text>: {getContactDetail.payload.lastName}</Text>
              <Text>: {getContactDetail.payload.age}</Text>
            </View>
          </View>
        </Fragment>
      )}
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  deleteContact: state.contact.deleteContact,
  getContactDetail: state.contact.getContactDetail,
});

//Map your action creators to your props.
const mapDispatchToProps = {
  deleteContactRequest: deleteContact,
  getContactRequest: getContact,
  getContactDetailRequest: getContactDetail,
};

//export your list as a default export
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContactDetailScreen);

const styles = StyleSheet.create({
  buttonRightContainer: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerRight: { flexDirection: 'row' },
  container: { padding: 20, flex: 1 },
  content: { alignItems: 'center' },
  images: { width: 100, height: 100, borderRadius: 50 },
  contentName: { flexDirection: 'row', paddingVertical: 20 },
  nameDetail: { fontWeight: 'bold', fontSize: 20, color: 'black' },
  contentDetail: { flexDirection: 'row', paddingTop: 20 },
  textName: { width: 100 },
  flex: { flex: 1 },
  avatarContainerStyle: { backgroundColor: 'grey', borderRadius: 50 },
  avatarStyle: { borderRadius: 50 },
});
