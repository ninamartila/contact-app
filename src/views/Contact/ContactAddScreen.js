import React, { useEffect, Fragment, useRef } from 'react';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Yup from 'yup';
import { Input } from 'react-native-elements';
import {
  getContact,
  getContactDetail,
  postContact,
  updateContact,
} from '../../redux/ContactThunk';
import { isEmptyOrSpaces } from '../../lib/helper';

const ContactAddScreen = (props) => {
  const navigation = useNavigation();
  const {
    postContactRequest,
    route,
    updateContactRequest,
    getContactDetailRequest,
    getContactRequest,
  } = props;
  const formikRef = useRef();
  const initValues = route.params;

  useEffect(() => {
    navigation.setOptions({
      title: initValues ? 'Edit Contact' : 'Add Contact',
      headerRight: () => (
        <TouchableOpacity
          onPress={() => {
            if (formikRef && formikRef.current) {
              formikRef.current.submitForm();
            }
          }}
          style={styles.buttonRightContainer}
        >
          <Icon name="check" size={30} />
        </TouchableOpacity>
      ),
    });
  }, []);

  const onSubmit = (values) => {
    console.log({ initValues, values });
    if (initValues && initValues.id) {
      updateContactRequest(initValues.id, values, () => {
        navigation.goBack();
        getContactDetailRequest(initValues.id);
        getContactRequest();
      });
    } else {
      postContactRequest(values, () => {
        navigation.goBack();
        getContactRequest();
      });
    }
  };

  return (
    <View style={styles.container}>
      <Formik
        innerRef={formikRef}
        initialValues={{
          firstName: (initValues && initValues.firstName) || '',
          lastName: (initValues && initValues.lastName) || '',
          age: (initValues && initValues.age) || '',
          photo: 'N/A',
        }}
        validationSchema={Yup.object().shape({
          firstName: Yup.string()
            .matches(/^\S*$/, 'First Name is not valid')
            .required('First Name is required'),
          lastName: Yup.string()
            .matches(/^\S*$/, 'Last Name is not valid')
            .required('Last Name is required'),
          age: Yup.number()
            .max(100, "Age can't be more than 100")
            .required('Age is required'),
        })}
        onSubmit={onSubmit}
      >
        {({ setFieldValue, values, errors }) => (
          <Fragment>
            <Input
              label="First Name"
              value={values.firstName}
              errorMessage={errors.firstName}
              onChangeText={(text) => setFieldValue('firstName', text)}
            />
            <Input
              label="Last Name"
              value={values.lastName}
              errorMessage={errors.lastName}
              onChangeText={(text) => setFieldValue('lastName', text)}
            />
            <Input
              label="Age"
              value={values.age.toString()}
              errorMessage={errors.age}
              keyboardType="number-pad"
              onChangeText={(text) =>
                setFieldValue(
                  'age',
                  !isEmptyOrSpaces(text) ? parseInt(text, 10) : text,
                )
              }
            />
          </Fragment>
        )}
      </Formik>
    </View>
  );
};

const mapStateToProps = (state) => ({
  postContact: state.contact.postContact,
  updateContact: state.contact.updateContact,
});

//Map your action creators to your props.
const mapDispatchToProps = {
  postContactRequest: postContact,
  updateContactRequest: updateContact,
  getContactDetailRequest: getContactDetail,
  getContactRequest: getContact,
};

//export your list as a default export
export default connect(mapStateToProps, mapDispatchToProps)(ContactAddScreen);

const styles = StyleSheet.create({
  buttonRightContainer: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: { margin: 10 },
  textForm: { fontWeight: 'bold' },
  textInput: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderColor: '#cfcfcf',
    borderRadius: 12,
    padding: 10,
  },
});
