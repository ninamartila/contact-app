import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import {
  StyleSheet,
  FlatList,
  View,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { getContact } from '../../redux/ContactThunk';
import ContactItem from '../../components/ContactItem';

const ContactListScreen = (props) => {
  const { getContact, getContactRequest } = props;
  const navigation = useNavigation();

  useEffect(() => {
    getContactRequest();
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('ContactAdd');
          }}
          style={styles.buttonRightContainer}
        >
          <Icon name="add" size={30} />
        </TouchableOpacity>
      ),
    });
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={getContact.fetching}
            onRefresh={() => {
              getContactRequest();
            }}
          />
        }
        data={getContact.payload || []}
        renderItem={({ item, index }) => (
          <ContactItem
            item={item}
            onPress={() => {
              navigation.navigate('ContactDetail', { item });
            }}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  getContact: state.contact.getContact,
});

//Map your action creators to your props.
const mapDispatchToProps = {
  getContactRequest: getContact,
};

//export your list as a default export
export default connect(mapStateToProps, mapDispatchToProps)(ContactListScreen);

const styles = StyleSheet.create({
  buttonRightContainer: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: { height: '100%', flex: 1 },
});
